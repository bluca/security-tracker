CVE-2021-32718
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-32719
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-22116
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2018-1279
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-3654
	[bullseye] - nova 2:22.2.2-1+deb11u1
CVE-2022-27240
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-29967
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-32096
	[bullseye] - rhonabwy 0.9.13-3+deb11u2
CVE-2022-28737
	[bullseye] - shim 15.6-1~deb11u1
CVE-2021-24119
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2021-44732
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2022-37186
	[bullseye] - lemonldap-ng 2.0.11+ds-4+deb11u2
CVE-2021-23450
	[bullseye] - dojo 1.15.4+dfsg1-1+deb11u1
CVE-2022-2255
	[bullseye] - mod-wsgi 4.7.1-3+deb11u1
CVE-2022-38529
	[bullseye] - tinyexr 1.0.0+dfsg-1+deb11u1
CVE-2022-34300
	[bullseye] - tinyexr 1.0.0+dfsg-1+deb11u1
CVE-2022-40320
	[bullseye] - libconfuse 3.3-2+deb11u1
CVE-2022-37616
	[bullseye] - node-xmldom 0.5.0-1+deb11u1
CVE-2022-3517
	[bullseye] - node-minimatch 3.0.4+~3.0.3-1+deb11u1
CVE-2022-42906
	[bullseye] - powerline-gitstatus 1.3.2-0+deb11u1
CVE-2021-42260
	[bullseye] - tinyxml 2.6.2-4+deb11u1
CVE-2021-46848
	[bullseye] - libtasn1-6 4.16.0-2+deb11u1
CVE-2021-42388
	[bullseye] - clickhouse 18.16.1+ds-7.2+deb11u1
CVE-2021-42387
	[bullseye] - clickhouse 18.16.1+ds-7.2+deb11u1
CVE-2021-43305
	[bullseye] - clickhouse 18.16.1+ds-7.2+deb11u1
CVE-2021-43304
	[bullseye] - clickhouse 18.16.1+ds-7.2+deb11u1
CVE-2022-2996
	[bullseye] - python-scciclient 0.8.0-2+deb11u1
CVE-2021-40241
	[bullseye] - xfig 1:3.2.8-3+deb11u1
CVE-2022-37601
	[bullseye] - node-loader-utils 2.0.0-1+deb11u1
CVE-2022-37599
	[bullseye] - node-loader-utils 2.0.0-1+deb11u1
CVE-2022-37603
	[bullseye] - node-loader-utils 2.0.0-1+deb11u1
CVE-2022-21690
	[bullseye] - onionshare 2.2-3+deb11u1
CVE-2022-21689
	[bullseye] - onionshare 2.2-3+deb11u1
